import 'dart:convert';
import 'package:flutter/services.dart';

class Service{
  Future readChatJson() async {
    final String response = await rootBundle.loadString('assets/jsons/dummydataforChat.json');
    List data = await jsonDecode(response);
    return data;
  }

  Future readStatusJson() async {
    final String response = await rootBundle.loadString('assets/jsons/dummyjsonforStatus.json');
    List data = await jsonDecode(response);
    return data;
  }

  Future readCallJson() async {
    final String response = await rootBundle.loadString('assets/jsons/dummyCallJson.json');
    List data = await jsonDecode(response);

    return data;
  }


}