// ignore_for_file: prefer_const_constructors, avoid_unnecessary_containers, sized_box_for_whitespace

import 'package:flutter/material.dart';
import 'package:whatsapp_clone/constants/colors.dart';
import 'package:whatsapp_clone/services/json_read.dart';
import 'package:whatsapp_clone/widgets/status/create_status.dart';
import 'package:whatsapp_clone/widgets/status/single_status.dart';

class StatusScreen extends StatefulWidget {
  const StatusScreen({super.key});

  @override
  State<StatusScreen> createState() => _StatusScreenState();
}

class _StatusScreenState extends State<StatusScreen> {
  List jsonDataList = [];
  bool isLoading = false;

  void jsonData() async {
    setState(() {
      isLoading = true;
    });
    List list = await Service().readStatusJson();

    setState(() {
      jsonDataList = list;
      isLoading = false;
    });
  }

  @override
  void initState() {
    super.initState();
    jsonData();
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      backgroundColor: Colors.white,
      body: Center(
        child: SingleChildScrollView(
          physics: ScrollPhysics(),
          child: Column(
            crossAxisAlignment: CrossAxisAlignment.start,
            children: [
              //MY STATUS
              createStatus(),
              //DIVIDER
              Padding(
                padding: const EdgeInsets.symmetric(horizontal: 10),
                child: Divider(
                  thickness: 2,
                ),
              ),
              SizedBox(
                height: 10,
              ),
              //RECENT UPDATES
              Padding(
                padding: const EdgeInsets.symmetric(horizontal: 20),
                child: Text(
                  'Recent updates',
                  style: TextStyle(
                      fontWeight: FontWeight.bold,
                      fontSize: 16,
                      color: Colors.grey),
                ),
              ),
              
              //STATUS LIST
              ListView.builder(
                
                physics: NeverScrollableScrollPhysics(),
                shrinkWrap: true,
                itemCount: jsonDataList.length,
                itemBuilder: (context, index) {
                  return Column(
                    children: [
                      statusListTile(
                          name: jsonDataList[index]['name'],
                          stausTime: jsonDataList[index]['status_time'],
                          day: jsonDataList[index]['day'],
                          profilePhoto: jsonDataList[index]['profile_image']),
                    ],
                  );
                },
              ),
              SizedBox(
                height: 10,
              ),
              //VIEW UPDATES
              Padding(
                padding: const EdgeInsets.symmetric(horizontal: 20),
                child: Text(
                  'View updates',
                  style: TextStyle(
                      fontWeight: FontWeight.bold,
                      fontSize: 16,
                      color: Colors.grey),
                ),
              ),
             
              //VIEW STATUS LIST
              ListView.builder(
                physics: NeverScrollableScrollPhysics(),
                shrinkWrap: true,
                itemCount: jsonDataList.length,
                itemBuilder: (context, index) {
                  return Column(
                    children: [
                      statusListTile(
                          name: jsonDataList[index]['name'],
                          stausTime: jsonDataList[index]['status_time'],
                          day: jsonDataList[index]['day'],
                          profilePhoto: jsonDataList[index]['profile_image']),
                    ],
                  );
                },
              ),
            ],
          ),
        ),
      ),
      //INSERT IMAGE FOR STATUS
      floatingActionButton: Container(
        height: 75,
        width: 75,
        child: FittedBox(
          child: FloatingActionButton(
            onPressed: () {},
            backgroundColor: WappColors.baseColor,
            child: Icon(Icons.camera_alt),
          ),
        ),
      ),
    );
  }
}
