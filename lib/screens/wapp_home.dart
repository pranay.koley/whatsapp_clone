// ignore_for_file: prefer_const_constructors, prefer_const_literals_to_create_immutables, sized_box_for_whitespace

import 'package:flutter/material.dart';
import 'package:whatsapp_clone/constants/colors.dart';
import 'package:whatsapp_clone/screens/call_screen.dart';
import 'package:whatsapp_clone/screens/chat_screen.dart';
import 'package:whatsapp_clone/screens/communities_screen.dart';
import 'package:whatsapp_clone/screens/status_screen.dart';

class WappHome extends StatefulWidget {
  const WappHome({super.key});

  @override
  State<WappHome> createState() => _WappHomeState();
}

class _WappHomeState extends State<WappHome> with TickerProviderStateMixin {
  late TabController _tabController;
  @override
  void initState() {
    super.initState();
    _tabController = TabController(length: 4, vsync: this, initialIndex: 1);
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      backgroundColor: WappColors.lightBackgroundColor,
      
      
      body: NestedScrollView(headerSliverBuilder: (context, innerBoxIsScrolled) {
         return <Widget>[
            SliverAppBar(
              actions: [
          Padding(
            padding: const EdgeInsets.all(8.0),
            child: Icon(Icons.camera_alt_outlined),
          ),
          Padding(
            padding: const EdgeInsets.all(8.0),
            child: Icon(Icons.search_outlined),
          ),
          PopupMenuButton<String>(
            onSelected: (value) {
              print(value);
            },
            itemBuilder: (BuildContext contesxt) {
              var mypop = _tabController.index;
              if (mypop == 1) {
                return [
                PopupMenuItem(
                  value: "New group",
                  child: Text("New group"),
                ),
                PopupMenuItem(
                  value: "New broadcast",
                  child: Text("New broadcast"),
                ),
                PopupMenuItem(
                  value: "Whatsapp Web",
                  child: Text("Whatsapp Web"),
                ),
                PopupMenuItem(
                  value: "Starred messages",
                  child: Text("Starred messages"),
                ),
                PopupMenuItem(
                  value: "Settings",
                  child: Text("Settings"),
                ),
              ];
              }
              else if(mypop == 2){
                return [
                PopupMenuItem(
                  value: "Status privacy",
                  child: Text("Status privacy"),
                ),
                PopupMenuItem(
                  value: "Settings",
                  child: Text("Settings"),
                )];
              }
              return [
                PopupMenuItem(
                  value: "Clear call log",
                  child: Text("Clear call log"),
                ),
                PopupMenuItem(
                  value: "Settings",
                  child: Text("Settings"),
                )];
              
            },
          )
        ],
              backgroundColor: WappColors.baseColor,
              title: Text('WhatsApp'),
              pinned: true,
              floating: true,
              forceElevated: innerBoxIsScrolled,
              bottom: TabBar(
            indicatorColor: Colors.white,
            controller: _tabController,
            tabs: [
              Tab(
                child: Icon(Icons.people_alt),
              ),
              Tab(
                child: Text(
                  'Chat',
                  style: TextStyle(fontWeight: FontWeight.bold, fontSize: 16),
                ),
              ),
              Tab(
                child: Text(
                  'Status',
                  style: TextStyle(fontWeight: FontWeight.bold, fontSize: 16),
                ),
              ),
              Tab(
                child: Text(
                  'Call',
                  style: TextStyle(fontWeight: FontWeight.bold, fontSize: 16),
                ),
              ),
            ]),
            ),
          ];
      }, body: TabBarView(
        controller: _tabController,
        children: [
          WappCommunities(),
          ChatScreen(),
          StatusScreen(),
          CallScreen()
        ]),)
    );
  }
}
