// ignore_for_file: prefer_const_constructors, avoid_unnecessary_containers, sized_box_for_whitespace

import 'package:flutter/material.dart';
import 'package:whatsapp_clone/constants/colors.dart';
import 'package:whatsapp_clone/services/json_read.dart';
import 'package:whatsapp_clone/widgets/call/create_call_link.dart';
import 'package:whatsapp_clone/widgets/call/single_call.dart';


class CallScreen extends StatefulWidget {
  const CallScreen({super.key});

  @override
  State<CallScreen> createState() => _CallScreenState();
}

class _CallScreenState extends State<CallScreen> {
  List jsonDataList = [];
  bool isLoading = false;

  void jsonData() async {
    setState(() {
      isLoading = true;
    });
    List list = await Service().readCallJson();

    setState(() {
      jsonDataList = list;
      isLoading = false;
    });
  }

  @override
  void initState() {
    super.initState();
    jsonData();
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      backgroundColor: Colors.white,
      body: Center(
        child: SingleChildScrollView(
          physics: ScrollPhysics(),
          child: Column(
            crossAxisAlignment: CrossAxisAlignment.start,
            children: [
              //MY STATUS
              createCallLink(stausTime: 'Share a link for your WhatsApp call'),
              //DIVIDER

              SizedBox(
                height: 10,
              ),
              //RECENT
              Padding(
                padding: const EdgeInsets.symmetric(horizontal: 20),
                child: Text(
                  'Recent',
                  style: TextStyle(
                      fontWeight: FontWeight.bold,
                      fontSize: 16,
                      color: Colors.grey),
                ),
              ),
              SizedBox(
                height: 10,
              ),
              //CALL LIST
              ListView.builder(
                physics: NeverScrollableScrollPhysics(),
                shrinkWrap: true,
                itemCount: jsonDataList.length,
                itemBuilder: (context, index) {
                  return Column(
                    children: [
                      callListTile(
                          name: jsonDataList[index]['name'],
                          callTime: jsonDataList[index]['call_time'],
                          profilePhoto: jsonDataList[index]['profile_image']),
                    ],
                  );
                },
              ),
            ],
          ),
        ),
      ),
      //CREATE CALL
      floatingActionButton: Container(
        height: 75,
        width: 75,
        child: FittedBox(
          child: FloatingActionButton(
            onPressed: () {},
            backgroundColor: WappColors.baseColor,
            child: Icon(Icons.add_call),
          ),
        ),
      ),
    );
  }
}
