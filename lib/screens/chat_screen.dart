// ignore_for_file: prefer_const_constructors, avoid_unnecessary_containers

import 'package:flutter/material.dart';
import 'package:whatsapp_clone/constants/colors.dart';
import 'package:whatsapp_clone/services/json_read.dart';
import 'package:whatsapp_clone/widgets/chat/single_chat.dart';

class ChatScreen extends StatefulWidget {
  const ChatScreen({super.key});

  @override
  State<ChatScreen> createState() => _ChatScreenState();
}

class _ChatScreenState extends State<ChatScreen> {
  List jsonDataList = [];
  bool isLoading = false;

  void jsonData() async {
    setState(() {
      isLoading = true;
    });
    List list = await Service().readChatJson();

    setState(() {
      jsonDataList = list;
      isLoading = false;
    });
  }

  @override
  void initState() {
    super.initState();
    jsonData();
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      backgroundColor: Colors.white,
      body: Container(
          child: ListView.builder(
        itemCount: jsonDataList.length,
        itemBuilder: (context, index) {
          return chatListTile(
              name: jsonDataList[index]['name'],
              lastMessage: jsonDataList[index]['last_message'],
              profilePhoto: jsonDataList[index]['profile_image'],
              msg_time: jsonDataList[index]['msg_time']);
        },
      )),
      floatingActionButton: Container(
        height: 75,
        width: 75,
        child: FittedBox(
          child: FloatingActionButton(
            onPressed: () {},
            backgroundColor: WappColors.baseColor,
            child: Icon(Icons.chat_rounded),
          ),
        ),
      ),
    );
  }
}
