// ignore_for_file: prefer_const_constructors

import 'package:flutter/material.dart';
import 'package:status_view/status_view.dart';

Widget statusListTile(
    {required String name,
    required String stausTime,
    required String profilePhoto,
    required String day}) {
  return ListTile(
    title: Text(
      name,
      style: TextStyle(fontSize: 20),
    ),
    subtitle: Row(
      children: [
        Text(
          day,
          style: TextStyle(fontSize: 16),
        ),
        Text(
          stausTime,
          style: TextStyle(fontSize: 16),
        ),
      ],
    ),
    leading: StatusView(
            radius: 25,
            spacing: 15,
            strokeWidth: 3,
            indexOfSeenStatus: 2,
            numberOfStatus: 5,
            padding: 4,
            centerImageUrl: profilePhoto,
            seenColor: Colors.grey,
            unSeenColor: Colors.green,
          ),
  );
}
