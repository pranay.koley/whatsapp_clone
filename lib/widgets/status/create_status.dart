import 'package:flutter/material.dart';

// ignore_for_file: prefer_const_constructors
Widget createStatus(
    {
    String profilePhoto =
        'assets/images/blank-profile-picture-g1797637ad_640.png'}) {
  return ListTile(
    title: Text(
      'My status',
      style: TextStyle(fontSize: 20),
    ),
    subtitle: Text(
      'Tap to add status update',
      style: TextStyle(fontSize: 16),
    ),
    leading: Container(
        height: 60,
        width: 60,
        decoration: BoxDecoration(shape: BoxShape.circle),
        child: CircleAvatar(
          backgroundImage: AssetImage(profilePhoto),
          backgroundColor: Colors.blueGrey[200],
          radius: 25,
        )),
  );
}
