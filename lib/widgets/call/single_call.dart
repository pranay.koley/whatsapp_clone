// ignore_for_file: prefer_const_constructors

import 'package:flutter/material.dart';
import 'package:whatsapp_clone/constants/colors.dart';

Widget callListTile(
    {required String name,
    required String callTime,
    required String profilePhoto}) {
  return ListTile(
    trailing: Icon(Icons.call, color: WappColors.baseColor,),
    title: Text(
      name,
      style: TextStyle(fontSize: 20),
    ),
    subtitle: Row(
      children: [
        Icon(Icons.call_received_outlined, color: Colors.green,size: 16,),
      SizedBox(width: 10,),
        Text(callTime, style: TextStyle(
      fontSize: 16))
      ],
    ),
    leading: Container(
        height: 60,
        width: 60,
        decoration: BoxDecoration(shape: BoxShape.circle),
        child: CircleAvatar(
          backgroundImage: AssetImage(profilePhoto),
          backgroundColor: Colors.blueGrey[200],
          radius: 25,
        )),
  );
}
