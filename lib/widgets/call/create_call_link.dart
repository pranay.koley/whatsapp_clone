import 'package:flutter/material.dart';
import 'package:whatsapp_clone/constants/colors.dart';

// ignore_for_file: prefer_const_constructors
Widget createCallLink(
    {
    required String stausTime,
    String profilePhoto =
        'assets/images/blank-profile-picture-g1797637ad_640.png'}) {
  return ListTile(
    title: Text(
      'Create call link',
      style: TextStyle(fontSize: 20),
    ),
    subtitle: Text(
      stausTime,
      style: TextStyle(fontSize: 16),
    ),
    leading: Container(
        height: 60,
        width: 60,
        decoration: BoxDecoration(shape: BoxShape.circle),
        child: CircleAvatar(
          backgroundColor: WappColors.baseColor,
          radius: 25,
          child: Icon(Icons.link),
        )),
  );
}
