// ignore_for_file: prefer_const_constructors

import 'package:flutter/material.dart';

Widget chatListTile(
    {required String name,
    required String lastMessage,
    required String profilePhoto,
    required String msg_time}) {
  return ListTile(
    
    title: Row(
      mainAxisAlignment: MainAxisAlignment.spaceBetween,
      children: [
        Text(
          name,
          style: TextStyle(fontSize: 20),
        ),
         Text(msg_time),
      ],
    ),
    subtitle: Row(
      children: [
        Container(
            height: 15,
            width: 15,
            decoration: BoxDecoration(shape: BoxShape.circle),
            child: CircleAvatar(
              backgroundImage:
                  AssetImage('assets/images/icons8-double-tick-100.png'),
              backgroundColor: Colors.transparent,
              radius: 25,
            )),
        SizedBox(
          width: 5,
        ),
        SizedBox(
          
          child: Padding(
            padding: const EdgeInsets.all(10),
            child: Text(lastMessage,
            overflow: TextOverflow.ellipsis,
            maxLines: 1, style: TextStyle(fontSize: 16)),
          ),
        )
      ],
    ),
    leading: Container(
        height: 60,
        width: 60,
        decoration: BoxDecoration(shape: BoxShape.circle),
        child: CircleAvatar(
          backgroundImage: AssetImage(profilePhoto),
          backgroundColor: Colors.blueGrey[200],
          radius: 25,
        )),
  );
}
